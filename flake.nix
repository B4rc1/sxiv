{
  description = "My fork of sxiv";

  outputs = { self }: {
    overlay =
      final: prev: {
        sxiv = prev.sxiv.overrideAttrs (super: {
          src = self;
        });
      };
  };
}
