![sxiv](http://muennich.github.com/sxiv/img/logo.png "sxiv")

**Simple X Image Viewer**

fork of https://github.com/muennich/sxiv

My Customisations
-----------------

I made these Customisations, because I couldn't be bothered to use the command-line arguments,
so i just patched it suckless style into the source. I Changed:

  * Hide the bar by default
  * Play gifs by default

